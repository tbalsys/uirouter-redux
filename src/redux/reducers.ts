import { Action } from 'redux';
import { DECREASE, INCREASE } from './actions';


export interface CounterState {
  value: number
}

const initialState: CounterState = {
  value: 0,
};

const counter: (state: CounterState, action: Action<string>) => CounterState = (state = initialState, action) => {
  switch (action.type) {
    case INCREASE: {
      return {
        value: state.value + 1,
      };
    }
    case DECREASE: {
      return {
        value: state.value - 1,
      };
    }
    default:
      return state;
  }
};

export default {
  counter,
};
